#include <argparse/argparse.hpp>
#include <armadillo>
#include <iostream>

int main(int argc, char* argv[]) {
  // argparse::ArgumentParser program("puntos");
  // program.add_argument("input").scan<'i', int>();
  // try {
  //   program.parse_args(argc, argv);
  // } catch (const std::runtime_error& err) {
  //   std::cerr << err.what() << std::endl;
  //   std::cerr << program;
  //   std::exit(1);
  // }

  // auto input = program.get<int>("input");
  // std::cout << (input * input) << std::endl;

  int input{3};

  arma::mat A{4, 5, arma::fill::zeros};

  A.cols(0,0).fill(-1.0);

  arma::mat B{5, 4, arma::fill::zeros};
  B.cols(0,0).randu();

  arma::mat C = A * B;

  C.print();

  return 0;
}
