{
  stdenv,
  cmake,
  lib,
  #
  argparse,
  armadillo,
}:
stdenv.mkDerivation {
  name = "puntos";

  src = lib.cleanSource ./.;

  nativeBuildInputs = [
    cmake
  ];

  buildInputs = [
    argparse
    armadillo
  ];
}
